﻿function load_new_game() {
	$.getJSON( "php/json.php", { "action" : "load", "id" : $("#num-partie").val()}, function(data,t) {
	//	alert(data);
		cartes_table = $.evalJSON(data.cartes_table);
		cartes_tas = $.evalJSON(data.cartes_tas);
		cartes_actif = $.evalJSON(data.cartes_actif);
		aide_utilisee = parseInt(data.aide_utilisee);
		cartes_suppl = parseInt(data.cartes_suppl);
		duree_partie = parseInt(data.duree_partie);
		$("#num-partie").val(data.id_partie)
		init_set_load();
	//	alert("OK");
	}); 
}

function init_set_load() {
	k = 0;
	for(var i=1;i<=5; i++) {
		for(var j=1;j<=3; j++) {
			if(i==5 && !cartes_suppl) { break; }
			set = cartes_table[k++].split("-");
			if(set) {
				modif_carte(set,"t"+i+""+j);
			}
		}
	}
	for(i=0; i<cartes_actif.length; i++) {
		$("#img-"+cartes_actif[i]).toggleClass("actif");
	}
	nbre_set = trouve_set(cartes_table,-1).length;
	$("#n-dispo").text(nbre_set);
	if(nbre_set==0) {
		ajoute_cartes_suppl();
	}
	time = new Date();
	// On retire le temps déjà écoulé
	ref_temps = Math.floor(time.getTime()/1000) - duree_partie;
	$("#n-reste").text(nombre_cartes - cartes_tas.length - cartes_table.length);
	fin_partie();
}

function save_new_game() {
	time = new Date();
	$.post( "php/json.php?action=save", {
		"cartes_table"  : $.toJSON(cartes_table),
		"cartes_tas"    : $.toJSON(cartes_tas),
		"cartes_actif"  : $.toJSON(cartes_actif),
		"aide_utilisee" : aide_utilisee,
		"cartes_suppl"  : cartes_suppl,
		"duree_partie"  : Math.floor(time.getTime()/1000) - ref_temps}, function(data,r) {$("#num-partie").val(data)}); 
}


/*
		cartes_tas = $.evalJSON(data.cartes_tas);
		cartes_actif = $.evalJSON(data.cartes_actif);
		aide_utilisee = data.aide_utilisee;
		cartes_suppl = data.cartes_suppl;
		duree_partie = data.duree_partie;
	*/

/*
cartes_tas = new Array();
cartes_actif = new Array();
cartes_suppl = false;
num_aide = 0;
cartes_vide = new Array();
path_img = "png"; // sans / final
aide_utilisee = false;

*/

