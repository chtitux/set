/*
forme :
	0 : carré
	1 : rond
	2 : bizarre
couleur :
	0 : rouge
	1 : violet
	2 : vert
nombre :
	0 : 1
	1 : 2
	2 : 3
remplissage :
	0 : plein
	1 : vide
	2 : bandes
*/
forme = new Array("ca","ro","bi");
couleur =  new Array("ro","vi","ve");
nombre =  new Array("1","2","3");
remplissage = new Array("pl", "vi", "ba");
cartes_table = new Array();
cartes_tas = new Array();
cartes_actif = new Array();
cartes_suppl = 0;
num_aide = 0;
cartes_vide = new Array();
path_img = "png"; // sans / final
aide_utilisee = 0;
user = "";

chgt = 3;
nombre_cartes = Math.pow(chgt,4);
nombre_cartes_table = 12;

function is_valid_set(carte1, carte2, carte3) {
	if(((carte1==carte2) && (carte1==carte3)) || (carte1 + carte2 + carte3 == 6)) {
		return true;
	} else {
		return false;
	}
}

function verif_set(cartes) {
	var is_set = true;
	for(var i=0; i<=6; i=i+2) {
		var test_set = new Array(
				parseInt(cartes[0].substr(i,1)),
				parseInt(cartes[1].substr(i,2)),
				parseInt(cartes[2].substr(i,2)));
		if(! (((test_set[0] == test_set[1]) && (test_set[0] == test_set[2])) 
			|| (test_set[0] + test_set[1] + test_set[2] == 6))) {
			is_set = false;
		}
	}
	return is_set;
}

function init_set() {
	for(var i=1;i<=4; i++) {
		for(var j=1;j<=3; j++) {
			var set = nouvelle_carte();
			if(set) {
				modif_carte(set,"t"+i+""+j);
			}
		}
	}
	nbre_set = trouve_set(cartes_table,-1).length;
	$("#n-dispo").text(nbre_set);
	if(nbre_set==0) {
		ajoute_cartes_suppl();
	}
	time = new Date();
	ref_temps = Math.floor(time.getTime()/1000);
	$("#n-reste").text(nombre_cartes - cartes_tas.length - cartes_table.length);
	fin_partie();
}

function clique_carte(carte) {
	carte.toggleClass("actif");
	if(carte.hasClass("actif")) {
		cartes_actif.push(carte.attr("id").substr(4));
		if(cartes_actif.length==3) {
			trois_cartes();
		}
	} else {
		cartes_actif.splice(jQuery.inArray(carte.attr("id").substr(4), cartes_actif), 1);
	}
}

function set_rand() {
	return Math.floor(1+Math.random()*chgt);
}

function nouvelle_carte() {
	// Si cartes posées + cartes table > cartes en tout => pas de nouvelle carte
	if(cartes_tas.length + cartes_table.length >= nombre_cartes) {
		return false;
	} else {
		set = new Array(set_rand(),set_rand(),set_rand(),set_rand());
		while(cartes_table.in_array(chaine_set(set)) || cartes_tas.in_array(chaine_set(set))) {
			set = new Array(set_rand(),set_rand(),set_rand(),set_rand());
		}
		cartes_table.push(chaine_set(set));
		return set;
	}
}

function chaine_set(set) {
	return set.join("-");
}

function img_carte(set) {
	return forme[set[0]-1]+"-"+couleur[set[1]-1]+"-"+nombre[set[2]-1]+"-"+remplissage[set[3]-1]+".png";
}

function trois_cartes() {
	if(verif_set(cartes_actif)) {
		// alert("Bravo !");
		if(aide_utilisee == false) {
			message("Bravo !",5000);
			$("#n-sets").text(parseInt($("#n-sets").text())+1);
		}
		// ajouter cartes au tas
		for(var i=0; i<=2; i++) {
			// ajoute au tas
			cartes_tas.push(cartes_actif[i]);
			// enleve les cartes sur la table
			cartes_table.splice(jQuery.inArray(cartes_actif[i], cartes_table), 1);
			// ajouter nouvelles cartes si cartes_suppl = false
			if(cartes_suppl == false) {
				set = nouvelle_carte();
				if(set.length == 4) {
					$("#img-"+cartes_actif[i]).attr({
							"id"	: "img-"+chaine_set(set),
							"alt"	: img_carte(set),
							"src"	: path_img+"/"+img_carte(set)}).removeClass("actif");
				} else {
					// Il n'y a plus de cartes dispo
					$("#img-"+cartes_actif[i]).remove();
				}
			} else {
				// On enlève les cartes supplementaires
				id_vide = parseInt($("#img-"+cartes_actif[i]).parent().attr("id").substr(1,2));
				if(id_vide < 50) {
					cartes_vide.push(id_vide);
				}
				$("#img-"+cartes_actif[i]).remove();
			}
		}
		if(cartes_suppl) {
			// Pour les emplacement cartes suppl.
			for(i=1; i<= 3; i++) {
				// Si il est plein
				if($("#t5"+i).children().length == 1) {
					// Deplacer la carte vers cartes_vide[0]
					set = $("#t5"+i).children().remove().attr("id").substr(4).split("-");
					modif_carte(set,"t"+cartes_vide[0]);
					cartes_vide.splice(0,1);
				}
			}
			cartes_suppl = 0;
		}
		$(".aide").removeClass("aide");
		aide_utilisee = 0;
		// vider carte_actif
		cartes_actif = new Array();
		num_aide = 0;
		nbre_set = trouve_set(cartes_table,-1).length;
		$("#n-dispo").text(nbre_set);
		if(nbre_set==0) {
			ajoute_cartes_suppl();
		}
		fin_partie();
		$("#n-reste").text(nombre_cartes - cartes_tas.length - cartes_table.length);
	} else {
		alert("Ceci n'est pas un Set!");
		$("#n-err").text(parseInt($("#n-err").text())+1);
		cartes_actif = new Array();
		$(".actif").removeClass("actif");
	}
}

function trouve_set(cartes, nombre) {
	sets_valides = new Array();
	for(i=0; i<= cartes.length - 3; i++) {
		for(j=i+1; j<= cartes.length - 2; j++) {
			for(k=j+1; k<= cartes.length - 1; k++) {
				if(verif_set([cartes[i], cartes[j], cartes[k]])) {
					sets_valides.push([i, j, k]);
//					sets_valides.splice(Math.floor(Math.random()*sets_valides.length), 0, [i, j, k]);
					if(sets_valides.length == nombre) { return sets_valides; }
				}
			}
		}
	}
	return sets_valides;
}

function ajoute_cartes_suppl() {
//	nombre_ajout = Math.min(3,nombre_cartes - (cartes_tas.length + cartes_table.length));
	for(i=1; i<= 3; i++) {
		var set = nouvelle_carte();
		if(set) {
			modif_carte(set,"t5"+i)
		} else {
			break;
		}
	}
	if(i-1 == 3 ) { cartes_suppl = 1; }
	$("#n-dispo").text(trouve_set(cartes_table,-1).length);
	$("#n-reste").text(nombre_cartes - cartes_tas.length - cartes_table.length);
	// i-1 : nombre de cartes ajoutées
	return i-1;
}

// 
function triplet_to_cartes(triplet, cartes) {
	if(triplet.length>=1) {
		return [cartes[triplet[0]], cartes[triplet[1]], cartes[triplet[2]]];
	} else {
		return Array();
	}
}

function aide_set(cartes_set, vite) {
	$(".aide").removeClass("aide");
	if(cartes_set.length==0) { return false; }
	for(i=0; i<=2; i++) {
		$("#img-"+cartes_set[i]).addClass("aide");
		if(vite) {
			cartes_actif.push(cartes_set[i]);
		}
	}
	if(vite) { trois_cartes(); }
}

function aide(triche) {
	sets = trouve_set(cartes_table,-1);
	if(sets.length==0) { return false; }
	aide_utilisee = 1;
	if(!triche) { num_aide = (1 + num_aide) % sets.length; }
	cartes = triplet_to_cartes(sets[num_aide],cartes_table);
	aide_set(cartes	,triche);
}

function message(mess,duree_affiche) {
	time = new Date();
	id_message = time.getTime();
	$("#message").append("<li id='msg-"+id_message+">"+mess+"</li>");
	if(duree_affiche>0) {
		setTimeout(function(){$("#msg-"+id_message).fadeOut("slow");},duree_affiche );
	}
}

function fin_partie() {
	if(trouve_set(cartes_table,1)==0) {
		affiche_temps();
		alert("Partie terminée. Plus aucun Set! n'est présent sur la table");
		$("#replay").show("slow");
	}
}

function modif_carte(set,id) {
	$("#"+id).html("<img/>").children().attr({
		"src"	: path_img+"/"+img_carte(set),
		"class"	: "carte",
		"id"	: "img-"+chaine_set(set),
		"alt"	: img_carte(set)});
	$("#img-"+chaine_set(set)).click(function () {
		clique_carte($(this));
	});
}

function affiche_temps() {
	time = new Date();
	diff_time = Math.floor(time.getTime()/1000) - ref_temps;
	diff_time_text = "";
	jours = Math.floor(diff_time / (60*60*24)); diff_time -= 60*60*24*jours;
	if(jours) { diff_time_text = jours+"j. "; }
//	if(jours>=2) { diff_time_text += "s"; }
	heures = Math.floor(diff_time / (60*60)); diff_time -= 60*60*heures;
	if(heures) { diff_time_text += heures+"h. "; }
//	if(heures>=2) { diff_time_text += "s"; }
	minutes = Math.floor(diff_time / 60); diff_time -= 60*minutes;
	if(minutes) { diff_time_text += minutes+"min. "; }
//	if(minutes>=2) { diff_time_text += "s"; }
	secondes = diff_time; diff_time -= secondes;	
	if(secondes) { diff_time_text += secondes+"s."; }
//	if(secondes>=2) { diff_time_text += "s"; }		
	if(diff_time_text != "") {
		$("#temps").text("Partie terminée en "+diff_time_text);
	}
//	timeout = setTimeout(affiche_temps,60000);
}