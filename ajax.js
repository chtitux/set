﻿function load_new_game(id_jeu) {
	// alert(id_jeu);
	$.getJSON( "json.php", { "action" : "load", "id" : id_jeu}, function(data,t) {
	//	alert(data);
		cartes_table = $.evalJSON(data.cartes_table);
		cartes_tas = $.evalJSON(data.cartes_tas);
		cartes_actif = $.evalJSON(data.cartes_actif);
		aide_utilisee = parseInt(data.aide_utilisee);
		cartes_suppl = parseInt(data.cartes_suppl);
		duree_partie = parseInt(data.duree_partie);
//		$("#num_partie").val(data.id_partie)
		init_set_load();
	//	alert("OK");
	}); 
}

function init_set_load() {
	k = 0;
	$(".carte").remove();
	for(var i=1;i<=5; i++) {
		for(var j=1;j<=3; j++) {
			if(i==5 && !cartes_suppl) { break; }
			set = cartes_table[k++].split("-");
			if(set) {
				modif_carte(set,"t"+i+""+j);
			}
		}
	}
	for(i=0; i<cartes_actif.length; i++) {
		$("#img-"+cartes_actif[i]).toggleClass("actif");
	}
	nbre_set = trouve_set(cartes_table,-1).length;
	$("#n-dispo").text(nbre_set);
	if(nbre_set==0) {
		ajoute_cartes_suppl();
	}
	time = new Date();
	// On retire le temps déjà écoulé
	ref_temps = Math.floor(time.getTime()/1000) - duree_partie;
	$("#n-reste").text(nombre_cartes - cartes_tas.length - cartes_table.length);
	fin_partie();
}

function save_game(id_jeu) {
	time = new Date();
	if(id_jeu) { action = "save"; } else { action = "new"; }
	$.post( "json.php?action="+action, {
		"id"			: id_jeu,
		"cartes_table"  : $.toJSON(cartes_table),
		"cartes_tas"    : $.toJSON(cartes_tas),
		"cartes_actif"  : $.toJSON(cartes_actif),
		"aide_utilisee" : aide_utilisee,
		"cartes_suppl"  : cartes_suppl,
		"duree_partie"  : Math.floor(time.getTime()/1000) - ref_temps,
		"user"			: user},
		function(data,r) {
			$("#num_partie").val(data.id_jeu);
			message("Partie "+data.id_jeu+" sauvegardée",1000);
		}, "json"); 
}

function login() {
	$.post( "json.php?action=login", {
		"ajax"		: true
		,"user"		: $("#user").val()
//		,"password"	: SHA256($("#password").val())
		},
		function(data,r) {
			if(parseInt(data.games)) {
				$("#choose-partie").val(parseInt(data.games));
				$("#load_prev_game").show();
			}
			$("#login").hide("slow");
			$("#parties").show("slow");
			user = data.user;
			 message(data.message,2000);
		}, "json");
}

function login_success_password(parties) {
	$("#login").hide("slow");
	$("#parties").show("slow");
	for(i=0; i<parties.length; i++) {
		$("#choose-partie").append("<option value='"+parties[i]+"'>Partie "+parties[i]+"</option>");
	}
	$("#parties").show("slow");
}