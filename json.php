<?php
header('Content-type: text/html; charset=UTF-8');
session_start();

include("static/config.inc.php");
include("static/lib.inc.php");
connect_db();
/**
	Structure :
	[ "cartes_table" : [],	
	  "cartes_tas"	 : [],
	  "cartes_actif" : [],
	  "cartes_suppl" = true/false,
	  "aide_utilisee" = true/false,
	  "temps" = secondes ]
*/

/*
$jeu = Array(
	  "cartes_table" => '["1-3-1-2","1-1-2-3","3-2-1-1","2-2-3-3","2-2-1-3","3-3-3-2","1-3-1-3","3-3-2-1","1-2-3-1","3-3-2-2","1-3-2-1","3-3-3-3"]',	
	  "cartes_tas"	 => '["2-3-2-3","2-3-3-1","2-3-1-2","3-3-1-1","3-1-3-1","3-2-2-1","3-1-2-2","2-2-2-2","1-3-2-2"]',
	  "cartes_actif" => "[]", // '["3-3-3-3","3-2-1-1","1-1-2-3"]',
	  "cartes_suppl" => false,
	  "aide_utilisee" => true,
	  "duree_partie" => 150545 );
*/

switch($_GET["action"]) {
	case "load":
		if(0 == ($id = intval($_GET["id"]))) {
			$query = "SELECT max(id) FROM ".TABLE_GAMES;
			$result = mysql_query($query);
			$id = mysql_result($result,0);
		}
		$query = "SELECT *,DATE_FORMAT(date,'%d/%m/%Y, %h:%i:%s') AS datefr FROM `".TABLE_GAMES."` WHERE `id` = ".intval($id)." LIMIT 1";
		$result = mysql_query($query);
		print mysql_error();
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$jeu = Array(
				"cartes_table" => $row["cartes_table"],	
				"cartes_tas"	 => $row["cartes_tas"],
				"cartes_actif" => $row["cartes_actif"], /* '["3-3-3-3","3-2-1-1","1-1-2-3"]', */
				"cartes_suppl" => $row["cartes_suppl"],
				"aide_utilisee" => $row["aide_utilisee"],
				"duree_partie" => $row["duree_partie"],
				"id_partie" => $row["id"]);
		}
		echo json_encode($jeu);
		break;
	case "new":
		   $query = sprintf("INSERT INTO `".TABLE_GAMES.
          "` (`id`, `cartes_table`, `cartes_tas`, `cartes_actif`, `aide_utilisee`, `cartes_suppl`, `duree_partie`, `date`			, `users` ) VALUES
             (NULL, '%s'		  , '%s'		, '%s'			, '%s'			 , '%s'			 , '%s'			 , CURRENT_TIMESTAMP, NULL	  )",
			mysql_real_escape_string(stripslashes($_POST['cartes_table']))
			,mysql_real_escape_string(stripslashes($_POST['cartes_tas']))
			,mysql_real_escape_string(stripslashes($_POST['cartes_actif']))
			,intval($_POST['aide_utilisee'])
			,intval($_POST['cartes_suppl'])
			,intval($_POST['duree_partie'])
//			,mysql_real_escape_string($_POST['timestamp'])
//			,mysql_real_escape_string($_POST['users'])
			);
//		print $query;
//		print_r($_POST);
		mysql_query($query);
		echo json_encode(
			Array("result"	=> mysql_errno(),
			"id_jeu"		=> mysql_insert_id(),
			"erreur_sql"	=> mysql_error()));
		break;
	case "login":
		$login = false;
		$query = sprintf("SELECT * FROM `".TABLE_USERS."` WHERE `user` = '%s' LIMIT 1",
			mysql_real_escape_string($_POST["user"]));
		$result = mysql_query($query);
		// print mysql_error();
		// print $query;
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
//			if(!$_POST['ajax']) $password = hash("sha256",$_POST["password"]);
//			else $password = $_POST['password'];
//			if($password == $row["password"] && mysql_num_rows($result) == 1) {
				$login = true;
				$_SESSION["games"] = $row["games"];
				$_SESSION["user"] = $row['user'];
				$_SESSION["login"] = true;
				$reponse = Array("result" => "success",
					"last_login" => $row['last_login'],
					"games"		 => $row["games"],
					"user"		 => htmlspecialchars($row['user']),
					"message"	 => "Bienvenue ".htmlspecialchars($row['user']));
//			}
		}
		// print "m $login ".mysql_num_rows($result)."m";
		if(!$login) {
			$reponse = Array("result" => "failure",
				"user"		 => htmlspecialchars($_POST["user"]),
				"message" => "Bonjour nouvel utilisateur ".htmlspecialchars($_POST["user"])); }
		echo json_encode($reponse);
		break;
	case "save":
		if(0 == ($id = intval($_POST["id"]))) {
			echo json_encode(Array("result" => "failure"));
			break;
		}
		$query = sprintf("UPDATE `".TABLE_GAMES."` SET
			`cartes_table` = '%s',
			`cartes_tas` = '%s',
			`cartes_actif` = '%s',
			`aide_utilisee` = '%s',
			`cartes_suppl` = '%s',
			`duree_partie` = '%s',
			`date` = CURRENT_TIMESTAMP,
			`users` = '%s' WHERE ".TABLE_GAMES.".`id` = '%s' LIMIT 1 ;\n",
			mysql_real_escape_string(stripslashes($_POST['cartes_table']))
			,mysql_real_escape_string(stripslashes($_POST['cartes_tas']))
			,mysql_real_escape_string(stripslashes($_POST['cartes_actif']))
			,intval($_POST['aide_utilisee'])
			,intval($_POST['cartes_suppl'])
			,intval($_POST['duree_partie'])
//			,mysql_real_escape_string("CURRENT_TIMESTAMP")
			,mysql_real_escape_string("utilisateurs")
			,intval($_POST['id']));
		$result = mysql_query($query);
		echo json_encode(Array("result" => "success", "query" => $query, "id_jeu" => intval($_POST['id'])));
		break;
}
?>